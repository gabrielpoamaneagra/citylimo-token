# CITYLIMO-TOKEN

## Install Notes
 - Add user data source for dsHandler config:
   ```
        "user": "postgres://user:pass@host:5432/user"
   ```
 - Add plugin in plugin-list.js:
   ```
        'citylimo-token'     : require('citylimo-token'),
   ```
 - For each route, in the plugin's  entrypoints.js, add config.auth = 'citylimo-token':
   ```
        {
           method : 'POST',
           path   : '/user/{id}/subscriptions',
           handler: require('./entrypoints/userSubscriptions'),
           config : {
               auth: 'citylimo-token'
           }
        }
   ```


## Usage

The current logged in use is available in ```request.auth.credentials```

