'use strict';

const authenticate  = require('./src/authenticate');

exports.registerPath = __dirname;

exports.register = (server, pluginOptions, next) => {
    server.auth.scheme('citylimo', function(server, options) {
        return {
            authenticate: authenticate(pluginOptions)
        };
    });

    server.auth.strategy('citylimo-token', 'citylimo');
    next();
};

exports.register.attributes = {
    name    : __dirname.substr(__dirname.lastIndexOf('/') + 1),
    multiple: false
};
