'use strict';

const Boom          = require('boom');
const Hoek          = require('hoek');
const semver        = require('semver');
const requestify    = require('requestify');

const getError = function(message, reason) {
    let boom = Boom.unauthorized(message);

    Hoek.merge(boom.output.payload, {
        type: 'system',
        reason: reason
    });

    return boom;
};

const getToken = function(authorization) {
    if (authorization) {
        let parts = authorization.split(' ');

        if (parts[0].trim().toLowerCase() != 'bearer') {
            throw new Error('Bearer tag not present');
        }

        return parts[1].trim();
    }

    throw new Error('Authorization header missing');
};

module.exports = function(options) {
    return function(request, reply) {
        let token   = null;

        try {
            token = getToken(request.headers['citylimo-token']);
        } catch(error) {
            reply(getError(error.message));

            return;
        }


        return requestify.get(options['auth-api'] + token)
            .catch(function(response) {
                reply(getError('Invalid token', 'invalid_token'));
            })
            .then((response) => {
                let user = response.getBody();

                if (user) {
                    if (user.status == 'active') {
                        reply.continue({
                            credentials: user
                        });
                    } else {
                        reply(getError('User disabled', 'account_disabled'));
                    }
                } else {
                    reply(getError('Invalid token', 'invalid_token'));
                }
            });
    }
};